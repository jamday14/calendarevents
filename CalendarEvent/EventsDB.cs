
using CalendarEvent.Models;
using Microsoft.EntityFrameworkCore;

namespace CalendarEvent
{
  public class EventsDB : DbContext
  {

    public EventsDB(DbContextOptions options) : base(options)
    { }

    public DbSet<Events> Events { get; set; }
  }
}
