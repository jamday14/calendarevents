export interface CalendarEvents {
  title: string;
  description: string;
  location: string;
  startDate: Date;
  endDate: Date;
}
