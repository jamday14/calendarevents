import { HttpClient } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { CalendarOptions } from '@fullcalendar/angular';
import { CalendarEvents } from '../models/calendar-events';
import * as moment from 'moment';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent implements OnInit {

  _baseUrl!: string;
  events: any[] = [];
  _http!: HttpClient;

  constructor(
    http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this._baseUrl = baseUrl;
    this._http = http;
  }

  ngOnInit() {
    this.populateEvents();
  }

  calendarOptions: CalendarOptions = {
    initialView: 'dayGridMonth',
    dateClick: this.handleDateClick.bind(this),
  };

  private populateEvents() {
    this._http.get<CalendarEvents[]>(this._baseUrl + 'calendarevents').subscribe(result => {
      this.events = result.map(x => { return { title: x.title, date: moment(x.startDate).format('YYYY-MM-DD') } });
      this.calendarOptions.events = this.events;
    }, error => console.error(error));

  }

  async handleDateClick(arg: any) {

    const event = {
      title: 'Jamal Event',
      description: 'Test Jamal Event',
      location: 'Manila',
      startDate: new Date(arg.dateStr),
      endDate: new Date(arg.dateStr)
    } as CalendarEvents;

    await this._http.post(this._baseUrl + 'calendarevents', event).toPromise();
    alert('Event has been successfully schedule on! ' + arg.dateStr);
    this.populateEvents();
  }
}

export class Event {
  title!: string;
  date!: string;

  constructor(title: string, date: string) {
    title = title;
    date = date;
  }
}
