﻿using CalendarEvent.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.OData.Query;

namespace CalendarEvent.Controllers;

[ApiController]
[Route("[controller]")]
public class CalendarEventsController : ControllerBase
{
  private readonly EventsDB _db;

  public CalendarEventsController(EventsDB db)
  {
    _db = db;
  }

  [HttpGet]
  [EnableQuery]
  public IQueryable<Events> Get()
  {
    return _db.Events;
  }

  [HttpPost]
  public async Task<ActionResult<Events>> Post(Events model)
  {
    await _db.Events.AddAsync(model);
    await _db.SaveChangesAsync();

    return Created($"/calendar-events/id{model.Id}", model);
  }
}
